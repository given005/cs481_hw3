﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Square
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
            //Setting up entry textboxes
            var entry = new Entry { Keyboard = Keyboard.Numeric, MaxLength = 1, FontSize = 40 };
            entry.TextColor = Color.Black;
            entry.BackgroundColor = Color.Beige;
        }
        //Navigation buttons clear text when moving form page to page
        async void OnNextPageButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
            Eval.Text = string.Empty;
            Cell3.Text = string.Empty;
            Cell4.Text = string.Empty;
            Cell5.Text = string.Empty;
            Cell6.Text = string.Empty;
            Cell7.Text = string.Empty;
            Cell9.Text = string.Empty;
        }

        async void OnPreviousPageButtonClicked(object sender, EventArgs e)
        {
            await Navigation.PopAsync();
            Eval.Text = string.Empty;
            Cell3.Text = string.Empty;
            Cell4.Text = string.Empty;
            Cell5.Text = string.Empty;
            Cell6.Text = string.Empty;
            Cell7.Text = string.Empty;
            Cell9.Text = string.Empty;
        }

        void Evaluate(object sender, EventArgs e)
        {
            double C1, C2, C3, C4, C5, C6, C7, C8, C9;
            //This checks to make sure the cells aren't empty so the program doesn't crash by passing
            //a null object
            if (this.Cell1.Text == null || this.Cell2.Text == null ||
                this.Cell3.Text == null || this.Cell4.Text == null ||
                this.Cell5.Text == null || this.Cell6.Text == null ||
                this.Cell7.Text == null || this.Cell8.Text == null ||
                this.Cell9.Text == null)
            {
                return;
            }
            C1 = Double.Parse(this.Cell1.Text);
            C2 = Double.Parse(this.Cell2.Text);
            C3 = Double.Parse(this.Cell3.Text);
            C4 = Double.Parse(this.Cell4.Text);
            C5 = Double.Parse(this.Cell5.Text);
            C6 = Double.Parse(this.Cell6.Text);
            C7 = Double.Parse(this.Cell7.Text);
            C8 = Double.Parse(this.Cell8.Text);
            C9 = Double.Parse(this.Cell9.Text);
            //Checks the sum of the cells
            if (C1 + C2 + C3 == 15 && C4 + C5 + C6 == 15 &&
                C7 + C8 + C9 == 15 && C1 + C4 + C7 == 15 &&
                C2 + C5 + C8 == 15 && C3 + C6 + C9 == 15 &&
                C1 + C5 + C9 == 15 && C3 + C5 + C7 == 15)
            {
                string correct = "Correct";
                this.Eval.Text = correct;
            }
            //This gives you incorrect if you repeat numbers in the square
            else if (C1 == C2 || C1 == C3 || C1 == C4 || C1 == C4 || C1 == C5 || C1 == C6 || C1 == C7
                || C1 == C8 || C1 == C9 || C2 == C3 || C2 == C4 || C2 == C5 || C2 == C6 || C2 == C7
                || C2 == C8 || C2 == C9 || C3 == C4 || C3 == C5 || C3 == C6 || C3 == C7 || C3 == C8
                || C3 == C9 || C4 == C5 || C4 == C6 || C4 == C7 || C4 == C8 || C4 == C9 || C5 == C6
                || C5 == C7 || C5 == C8 || C5 == C9 || C6 == C7 || C6 == C8 || C6 == C9 || C7 == C8
                || C7 == C9 || C8 == C9)
            {
                string incorrect = "Incorrect";
                this.Eval.Text = incorrect;
            }
            else
            {
                string incorrect = "Incorrect";
                this.Eval.Text = incorrect;
            }
        }
    }
}